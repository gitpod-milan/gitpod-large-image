FROM gitpod/workspace-full-vnc

COPY . .
RUN go run main.go --wait 2m --tick 1s
